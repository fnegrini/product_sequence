# -*- coding: utf-8 -*-

from openerp import models, fields, api

class product_product(models.Model):
    _inherit = "product.product"

    def create(self, vals):

		# Check if sequence exists and assign new number to product
        if vals.get('code', '') == '':
            vals['code'] = self.env['ir.sequence'].next_by_code('product.product') or ''

        return super(product_product, self).create(vals)
