# -*- coding: utf-8 -*-
{
    'name': 'Product unique reference',
    'images': ['static/description/main_screenshot.png'],
    'version': '10.0.0.1',
    'summary': 'Generates unique identifier for product reference',
    'category': 'Accounting',
    'author': 'Arqtios',
    'website': 'http://www.artios.com',
    'depends': [
        'base',
        'product',
    ],
    'data': [
        'data/product_sequence.xml',
    ],
    'installable': True,
}
